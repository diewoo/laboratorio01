package com.diewoo.labsesion0

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcess.setOnClickListener {
            var age = edtAge.text.toString()
            if(age.isEmpty()){
                this.msg("Ingrese su edad, por favor")
                return@setOnClickListener
            }

            tvResult.text= when(age.toInt()) {
                in  0..17 -> {
                    "Usted es menor de edad"
                }
                else -> {
                    "Usted es mayor de edad"
                }
            }
        }


    }
    fun Context.msg(message:String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }
}