package com.diewoo.laboratorio05

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.diewoo.laboratorio05.adapter.ContactAdapter
import com.diewoo.laboratorio05.model.Contact
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val contacts = mutableListOf<Contact>()
    lateinit var adapter: ContactAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progress.visibility = View.VISIBLE
        configureAdapter()
        showDialog()

    }
    private fun showDialog() {

        adapter = ContactAdapter(){
            val i = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + it.phoneNumber))
            try {
                startActivity(i)
            } catch (e: Exception) {
                // this can happen if the device can't make phone calls
                // for example, a tablet
            }
        }

        recyclerContact.adapter = adapter
        recyclerContact.layoutManager = LinearLayoutManager(this)

    }

    private fun configureAdapter() {
        adapter = ContactAdapter() {

        }
        recyclerContact.adapter = adapter
        recyclerContact.layoutManager = LinearLayoutManager(this)

        Handler(Looper.getMainLooper()).postDelayed({

            adapter.uptateList(loadDummyData())
            progress.visibility = View.GONE

        }, 5000)


    }

    private fun loadDummyData(): MutableList<Contact> {
        contacts.add(
            Contact(
                "Diego Renteria",
                "Ingeniero",
                "dgo250594@gmail.com",
                "936928847"
            )
        )
        contacts.add(
            Contact(
                "Miguel Bernedo",
                "Ingeniero",
                "mbernedo5@gmail.com",
                "912128847"
            )
        )
        contacts.add(
            Contact(
                "Christian Valencia",
                "Ingeniero",
                "cros410@gmail.com",
                "992028847"
            )
        )
        contacts.add(
            Contact(
                "Sebastian Rey",
                "Ingeniero",
                "sebas.rey@gmail.com",
                "989108822"
            )
        )
        contacts.add(
            Contact(
                "Alessandra Renteria",
                "Ingeniero",
                "ale.renteria@gmail.com",
                "939108822"
            )
        )
        return contacts
    }

}