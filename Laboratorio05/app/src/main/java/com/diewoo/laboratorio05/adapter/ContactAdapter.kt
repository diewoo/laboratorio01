package com.diewoo.laboratorio05.adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.diewoo.laboratorio05.R
import com.diewoo.laboratorio05.model.Contact
import kotlinx.android.synthetic.main.item.view.*
class ContactAdapter (
    private var contacts: MutableList<Contact> = mutableListOf(),
    private val itemCallBackContact: (item: Contact)-> Unit):
            RecyclerView.Adapter<ContactAdapter.ContactAdapterViewHolder>(){
    class ContactAdapterViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        fun bind(contact: Contact, itemCallBackContact: (item: Contact) -> Unit){

            itemView.tvNames.text = contact.names
            itemView.tvCharge.text = contact.charge
            itemView.tvEmail.text = contact.email
            itemView.tvWord.text = contact.names.substring(0,1)
            itemView.imgCall.setOnClickListener { itemCallBackContact(contact) }
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item,parent,false)
        return ContactAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    fun uptateList(contacts: MutableList<Contact>){
        this.contacts = contacts
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ContactAdapterViewHolder, position: Int) {

        val contact = contacts[position]

        holder.bind(contact,itemCallBackContact)


    }


}