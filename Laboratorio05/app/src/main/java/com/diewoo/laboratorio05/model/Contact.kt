package com.diewoo.laboratorio05.model

data class Contact (
    val names: String,
    val charge:String,
    val email:String,
    val phoneNumber: String,
)