package com.diewoo.laboratoriolistas.Interface

import androidx.appcompat.app.AlertDialog
import com.diewoo.laboratoriolistas.Model.Vehiculo

interface IList {
    fun createDialog(vehiculo: Vehiculo):AlertDialog
}