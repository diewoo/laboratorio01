package com.diewoo.laboratoriolistas.Model

data class Vehiculo(
    val name:String,
    val year: String,
    val description:String,
    val brand:String,
)