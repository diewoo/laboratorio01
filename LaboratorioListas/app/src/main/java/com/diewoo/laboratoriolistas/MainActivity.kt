package com.diewoo.laboratoriolistas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.diewoo.laboratoriolistas.Interface.IList
import com.diewoo.laboratoriolistas.Model.Vehiculo
import com.diewoo.laboratoriolistas.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),IList {
    var pickedBrand = ""
    var cont = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val brands = listOf("Kia", "Hyundai","Toyota","Fiat")
        val adapter = ArrayAdapter(this, R.layout.style_spinner, brands)
        spBrands.adapter = adapter
        spBrands.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                if (cont!=0){
                    pickedBrand = brands[position]
                }
                cont++

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
        btnConfirm.setOnClickListener {
            if(pickedBrand.isEmpty()){
                Toast.makeText(this,"Debe seleccionar una marca",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val name= binding.edtNames.text.toString()
            val year= binding.edtYear.text.toString()
            val description = binding.edtDescription.text.toString()
            val vehicle = Vehiculo(name,year,description,pickedBrand)
            createDialog(vehicle).show()
        }

    }
    override fun createDialog(vehicle:Vehiculo): AlertDialog {
        val alertDialog: AlertDialog
        val builder  = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val v: View = inflater.inflate(R.layout.dialogo, null)
        builder.setView(v)
        val btnNo: Button = v.findViewById(R.id.btn_aperture_no)
        val btnYes: Button = v.findViewById(R.id.btn_aperture_yes)
        val imgLogo: ImageView = v.findViewById(R.id.imgLogo)
        val tvData: TextView = v.findViewById(R.id.tvData)

        when (vehicle.brand) {
            "Kia" -> {
                imgLogo.setBackgroundResource(R.drawable.icon_kia)
            }
            "Hyundai" -> {
                imgLogo.setBackgroundResource(R.drawable.icon_hyundai)
            }
            "Toyota" -> {
                imgLogo.setBackgroundResource(R.drawable.icon_toyota)
            }
            "Fiat" -> {
                imgLogo.setBackgroundResource(R.drawable.icon_fiat)
            }
        }

        tvData.text = "Propietario: ${vehicle.name} \nMarca: ${vehicle.brand} \nAño: ${vehicle.year} \nProblema: ${vehicle.description}"

        alertDialog = builder.create()

        btnYes.setOnClickListener {
            pickedBrand = vehicle.brand
            Toast.makeText(this,"Reparar vehículo seleccionado \nmarca $vehicle.brand para su Reparación",Toast.LENGTH_SHORT).show()
            alertDialog.dismiss()
        }

        btnNo.setOnClickListener{
            alertDialog.dismiss()
        }

        return alertDialog
    }



}