package com.diewoo.laboratorio03

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        petRadioGroup.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = findViewById(checkedId)
                this.msg(" Se cambio por :" +
                        " ${radio.text}")
            })
        btnSend.setOnClickListener {
            val names = edtNames.text.toString()
            val age = edtAge.text.toString()

            if (names.isEmpty()) {

                this.msg("Debe ingresar el nombre")
                return@setOnClickListener
            }

            if (age.isEmpty()) {

                this.msg("Debe ingresar la edad")
                return@setOnClickListener
            }
            val result = StringBuilder()
            var selectedVacoon: CheckBox
            if (!chkParvorius.isChecked && !chkRabia.isChecked && !chkMoquillo.isChecked && !chkAdenovirus.isChecked && !chkHepatitis.isChecked) {
                this.msg("Debe seleccionar al menos una Vacuna")
                return@setOnClickListener
            }else{
                if (chkParvorius.isChecked){
                    selectedVacoon = findViewById(R.id.chkParvorius)
                    result.append ("\n"+chkParvorius.text.toString())
                }
                if (chkRabia.isChecked){
                    selectedVacoon = findViewById(R.id.chkRabia)
                    result.append ("\n"+chkRabia.text.toString())
                }
                if (chkMoquillo.isChecked){
                    selectedVacoon = findViewById(R.id.chkMoquillo)
                    result.append ("\n"+chkMoquillo.text.toString())
                }
                if (chkAdenovirus.isChecked){
                    selectedVacoon = findViewById(R.id.chkAdenovirus)
                    result.append ("\n"+chkAdenovirus.text.toString())
                }
                if (chkHepatitis.isChecked){
                    selectedVacoon = findViewById(R.id.chkHepatitis)
                    result.append ("\n"+ chkHepatitis.text.toString())
                }
            }

            var id: Int = petRadioGroup.checkedRadioButtonId
            val radio: RadioButton = findViewById(id)
            val bundle = Bundle()

            bundle.apply {
                putString("key_names", names)
                putString("key_age", age)
                putString("key_pet", radio.text.toString())
                putString("key_vacoons", result.toString())
            }

            val intent = Intent(this, PetInfoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }
    }

    fun Context.msg(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun radioButtonClick(view: View) {
        val radio: RadioButton = findViewById(petRadioGroup.checkedRadioButtonId)
    }

}