package com.diewoo.laboratorio03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_pet_info.*

class PetInfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_info)
        val bundle : Bundle? = intent.extras
        bundle?.let { bundleLibreDeNull ->

            val names =  bundleLibreDeNull.getString("key_names","Desconocido")
            val age = bundleLibreDeNull.getString("key_age","0")
            val pet = bundleLibreDeNull.getString("key_pet","")
            val vacoons = bundleLibreDeNull.getString("key_vacoons","")


            tvPetNames.text = "Nombres: $names"
            tvPetAge.text = "Edad: $age"
            tvPetType.text = "Mascota: $pet"
            tvPetVacoons.text = "Vacunas:\n $vacoons"

            when (pet) {
                "Perro" -> {
                    imgPet.setImageResource(R.drawable.dog);
                }
                "Gato" -> {
                    imgPet.setImageResource(R.drawable.cat);
                }
                else -> {
                    imgPet.setImageResource(R.drawable.rabbit);
                }
            }
        }

    }
}