package com.diewoo.lab02

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnVerify.setOnClickListener{

            val age = edtAge.text.toString()
            var generation = ""
            if(age.isEmpty()){
                this.msg("Ingrese su edad, por favor")
                return@setOnClickListener
            }
            when (age.toInt()) {

                in 1930..1948 -> {
                    generation = "Silent Generation"
                    imgFeature.setImageResource(R.drawable.austeridad)

                }
                in 1949..1968 -> {
                    generation = "Baby Boom"
                    imgFeature.setImageResource(R.drawable.ambicion)

                }
                in 1969..1980 -> {
                    generation = "Generation X"
                    imgFeature.setImageResource(R.drawable.obsesion)

                }
                in 1981..1993 -> {
                    generation = "Generation Y"
                    imgFeature.setImageResource(R.drawable.frustacion)
                }
                in 1994..2010 -> {
                    generation = "Generation Z"
                    imgFeature.setImageResource(R.drawable.irreverencia)
                }
                else -> {
                    generation = "N/D"
                    imgFeature.setImageResource(R.drawable.ic_launcher_background)
                }

            }

            tvGeneration.text = generation
        }

    }
    fun Context.msg(message:String) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }
}